# -*- coding: utf-8 -*-
##############################################################################
#
#    GNU Health: The Free Health and Hospital Information System
#
#                            CALENDAR PACKAGE
#
#    Copyright (C) 2008-2017  Luis Falcon <lfalcon@gnusolidario.org>
#    Copyright (C) 2011-2017  GNU Solidario <health@gnusolidario.org>
#    Copyright (C) 2011-2014  Sebastián Marró <smarro@thymbra.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import timedelta
from trytond.model import fields
from trytond.pool import PoolMeta

__all__ = ['Appointment']


class Appointment:
    __metaclass__ = PoolMeta
    __name__ = 'gnuhealth.appointment'
    appointment_date_end = fields.DateTime('End Date and Time', required=True,
        depends=['appointment_date'])

    @fields.depends('appointment_date')
    def on_change_appointment_date(self, name=None):
        if self.appointment_date:
            self.appointment_date_end = self.appointment_date + timedelta(hours=1)
